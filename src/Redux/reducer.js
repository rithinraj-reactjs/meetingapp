import Promice from 'es6-promise';

const LOGIN_PENDING = "LOGIN_PENDING";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGIN_ERROR = "LOGIN_ERROR";

function  setLoginPending(isLoginPending){
    return {

        type:LOGIN_PENDING,
        isLoginPending
    };
}

function  setLoginSuccess(isLoginSuccess){
    return {

        type:LOGIN_SUCCESS,
        isLoginSuccess
    };
}
function  setLoginError(loginError){
    return {

        type:LOGIN_ERROR,
        loginError
    };
}

export  function login(username,password){

    console.log(username);
    console.log(password);

    return dispatch => {
        dispatch(setLoginPending(true));
        dispatch(setLoginSuccess(false));
        dispatch(setLoginError(null));

        sendLoginRequest(username,password)
        .then(success =>{

            dispatch(setLoginPending(false));
            dispatch(setLoginSuccess(true));

        })
        .catch(err => {

            dispatch(setLoginPending(false));
            dispatch(setLoginError(err));

        });
        

    };

}



export default function reducer(state = {

    isLoginPending:false,
    isLoginSuccess:false,
    loginError:null

},action ){
    switch(action.type){
        case LOGIN_SUCCESS:
            return{
                ...state,
                isLoginSuccess:action.isLoginSuccess
            };
            case LOGIN_PENDING:
            return{
                ...state,
                isLoginPending:action.isLoginPending
            };
            case LOGIN_ERROR:
            return{
                ...state,
                loginError:action.loginError
            }
            default:
                 return state;
    }
}

function sendLoginRequest(username,password){
    return new Promice((resolve,reject) => {
        setTimeout(()=>{
            if(username === 'rithinraj@gmail.com' && password === '123456789'){
             
                return resolve(true);
            }else{
             
                return reject(new Error('Invalid Username or Password'));
            }

        },1000);
        
    })
}