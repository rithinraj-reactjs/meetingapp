import React, { Component } from 'react';
import './homePage.css'

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.myChangeHandler = this.myChangeHandler.bind(this);
        this.handle_home_Submit = this.handle_home_Submit.bind(this);

        this.state = { 
            name:'',
            email:'',
            date:''
         }
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;

        this.setState({[nam]: val});
    }



    handle_home_Submit = (event) => {
        event.preventDefault();
        localStorage.setItem('document',JSON.stringify(this.state));
    }


    componentDidMount() {
        this.documentData = JSON.parse(localStorage.getItem('document'));
     
        if (localStorage.getItem('document')) {
            this.setState({
                name: this.documentData.name,
                email: this.documentData.email,
                date: this.documentData.date
        })
    } else {
        this.setState({
            name: '',
            email: '',
            date: ''
        })
    }
    }

    render() { 
        return ( 
            <div className="modals">
                <h1>Home Page</h1>
                <form name="form" onSubmit={this.handle_home_Submit}>
                    <div className="modelsContent">
                        <div className="container">
                            <label className="floatLeft"><b>Participants Name:</b></label>
                            <input type="text" placeholder="Enter Participants Name" onChange={this.myChangeHandler} name="name" required />
                    
                            <label className="floatLeft"><b>Participants Email Id:</b></label>
                            <input type="email" placeholder="Enter Participants Email Id" onChange={this.myChangeHandler} name="email" required />

                            <label className="floatLeft"><b>Schedule Date:</b></label>
                            <input type="date" onChange={this.myChangeHandler} name="date" required />
                    
                            <button type="submit">Submit data</button>
                        </div>
                    </div>
                </form>
                <div>
                    <h1 className="formDisplayData">Participants Name: { this.state.name }</h1>
                    <h1 className="formDisplayData">Participants Email Id: { this.state.email }</h1>
                    <h1 className="formDisplayData">Schedule Date: { this.state.date }</h1>
                </div>

            </div>
         );
    }
}
 
export default HomePage;