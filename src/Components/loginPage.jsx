import React, { Component } from 'react';
import './loginPage.css'
import {login} from '../Redux/reducer';
import { connect } from 'react-redux';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
      
        let {username,password} = this.state
        this.props.login(username,password);
    }

    render() { 
        let{isLoginPending,isLoginSuccess,loginError} = this.props
        return ( 
            <div className="modals">
                <h1>Login Page</h1>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className="modelsContent">
                        <div className="container">
                            <label className="floatLeft"><b>Username:</b></label>
                            <input type="text" placeholder="Enter Username" name="username" onChange={e => this.setState({username:e.target.value})} required />
                    
                            <label className="floatLeft"><b>Password:</b></label>
                            <input type="password" placeholder="Enter Password" name="password" onChange={e => this.setState({password:e.target.value})} required />
                    
                            <button type="submit">Login</button>
                            {isLoginPending && <div>Please wait ...</div>}
                            {isLoginSuccess && <div>{this.props.history.push("/Home")}</div>}
                            {loginError && <div>Invalid Username or Password</div>}

                        </div>
                    </div>

                </form>

            </div>

         );
    }
}


const mapStateToProps = (state) =>{
    return{
        isLoginPending:state.isLoginPending,
        isLoginSuccess:state.isLoginSuccess,
        loginError:state.loginError

    };
}

const mapDispatchToProps = (dispatch) =>{
    return{
        login:(username,password) => dispatch(login(username,password))

    };
}
 
export default connect(mapStateToProps,mapDispatchToProps) (Login);