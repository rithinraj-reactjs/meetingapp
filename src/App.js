import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from './Components/loginPage';
import HomePage from './Components/HomePage/homePage';


function App() {
  return (
    <Router >
    <div className="App">
     <Switch>
     <Route path="/" exact component={Login}></Route>
     <Route path="/Home" component={HomePage}></Route>
     
     </Switch>
    </div>
    </Router>
  );
}

export default App;
